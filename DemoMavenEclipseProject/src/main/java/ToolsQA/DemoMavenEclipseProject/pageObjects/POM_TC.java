package ToolsQA.DemoMavenEclipseProject.pageObjects;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import ToolsQA.DemoMavenEclipseProject.Utility.Constant;
import ToolsQA.DemoMavenEclipseProject.appModule.SignIn_Action;




public class POM_TC {

private static WebDriver driver = null;

public static void main(String[] args) {
	
	
System.setProperty(Constant.driver, Constant.URL);

driver = new ChromeDriver();

driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

driver.get("http://www.store.demoqa.com");

// Use page Object library now

SignIn_Action.Execute(driver,Constant.Username,Constant.Password);

System.out.println(" Login Successfully, now it is the time to Log Off buddy.");

Home_Page.lnk_LogOut(driver).click(); 

driver.quit();

}

}