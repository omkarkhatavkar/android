package ToolsQA.DemoMavenEclipseProject.appModule;

import org.openqa.selenium.WebDriver;

import ToolsQA.DemoMavenEclipseProject.pageObjects.Home_Page;
import ToolsQA.DemoMavenEclipseProject.pageObjects.Login_Page;

public class SignIn_Action{

public static void Execute(WebDriver driver, String sUserName, String sPassword){


Home_Page.lnk_MyAccount(driver).click();

Login_Page.txtbx_UserName(driver).sendKeys(sUserName);

Login_Page.txtbx_Password(driver).sendKeys(sPassword);

Login_Page.btn_LogIn(driver).click();

}

}
