package ToolsQA.DemoMavenEclipseProject;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DataProviderTesting {
	
@DataProvider(name = "Capital")	

public Object[][] Testing(){
	
	return new Object[][] {{"omkar"}, {"priya"}};
	
}

@Test (dataProvider = "Capital")
public void f(String Value) throws Exception {
	  
	  String exePath = "E:\\ChromeDriver\\chromedriver.exe";
	   System.setProperty("webdriver.chrome.driver", exePath);
	   WebDriver driver = new ChromeDriver();
	   driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	   driver.manage().window().maximize();
	   driver.get("https://www.google.com");
	   WebElement Search = driver.findElement(By.name("q"));
	   Search.sendKeys(Value);
	   Thread.sleep(5000);
	   driver.close();
	   
  }
}
