package ToolsQA.DemoMavenEclipseProject;

public  class StaticCar {

	        public String sModel;
	        public int iHighestSpeed;
	        public static int iSteering;
	        public static int iWheel;
	        public static int iDoors;
	 
	static {
	        iSteering = 1;
	        iWheel = 4;
	        System.out.println("This block executed first");
	        }
	 
	 public static void DisplayCharacterstics() {
	        System.out.println("Number of Wheels in the Car: " + iWheel);
	        System.out.println("Number of Steering in the Car: " + iSteering);
	        }
	    }
