package ToolsQA.DemoMavenEclipseProject;

import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;

import java.io.FileOutputStream;

import org.apache.poi.xssf.usermodel.XSSFCell;

import org.apache.poi.xssf.usermodel.XSSFRow;

import org.apache.poi.xssf.usermodel.XSSFSheet;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;



public class ExcelReadWrite  {
        
   
  @Test
  public void f() throws Exception {
	 String Path = "E:\\Selenium Tests\\MavenProject\\src\\main\\java\\ToolsQA\\DemoMavenEclipseProject\\Utility\\TestData.xlsx";
	 String SheetName = "Fruit";
	 
	 //FileRead
	try {
	 FileInputStream ExcelFile = new FileInputStream(Path);
	 XSSFWorkbook ExcelWBook = new XSSFWorkbook(ExcelFile);
	 XSSFSheet ExcelWSheet = ExcelWBook.getSheet(SheetName);
	  
	  int rownum = ExcelWSheet.getLastRowNum();
	  
	  for(int i=0;i<rownum+1;i++) {
		  
	  int columnnum = ExcelWSheet.getRow(i).getLastCellNum();
	  
	  for(int j=0;j<columnnum;j++) {
		  System.out.print(ExcelWSheet.getRow(i).getCell(j).getStringCellValue()+"\t");
		  		}
	  System.out.println("\n");
	  }	
	  
              
          
          //FileWrite
	  FileOutputStream fileOut = new FileOutputStream(Path);

	  

	  for (int k=1;k<rownum+1;k++) {
		  XSSFRow  Row  = ExcelWSheet.getRow(k);
          XSSFCell    Cell = Row.getCell(2);
	      Cell.setCellValue("Pass");
          
	  }
	  ExcelWBook.write(fileOut);
      fileOut.flush();
      fileOut.close();
      ExcelWBook.close();
            
	}
	catch (Exception e) {
		throw(e);
	}
	 }
  }

