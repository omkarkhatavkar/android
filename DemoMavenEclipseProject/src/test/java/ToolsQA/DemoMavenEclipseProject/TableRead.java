package ToolsQA.DemoMavenEclipseProject;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class TableRead {
  @Test
  public void f() {
	  String URL = "http://demo.guru99.com/test/table.html";
	  String Path = "E:\\ChromeDriver\\chromedriver.exe";
	  System.setProperty("webdriver.chrome.driver",Path);
	  WebDriver driver = new ChromeDriver();
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  driver.manage().window().maximize();
	  driver.get(URL);
	  
	  
	  WebElement table = driver.findElement(By.tagName("table"));
	  List<WebElement> rows = table.findElements(By.tagName("tr"));
	  
	  for(int i=0;i<rows.size();i++) {
		  List<WebElement> columns = rows.get(i).findElements(By.tagName("td"));
		  
		  for (int j=0;j<columns.size();j++) {
			System.out.println(columns.get(j).getText()+"\t");  
		  }
		  System.out.println();
		  }
	  }
  }

