package ToolsQA.DemoMavenEclipseProject;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;


public class TestNGAnnotations {
	@Parameters({"Var1","Var2"})
	@Test (priority = 3 )
  public void Sum(int v1, int v2) {
  	int finalsum = v1 + v2;
      System.out.println("The final sum of the given values is " + finalsum);
  }
	
	@Parameters({"Var1","Var2"})
  @Test (dependsOnMethods = "Three")
  public void Two() {
	 
	  
	  System.out.println("Test Case two");
  }
  
  @Test(priority = 1)
  public void Three() {
	  
	  System.out.println("Test Case three");
  }
}
