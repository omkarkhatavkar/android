package ToolsQA.DemoMavenEclipseProject;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.AssertJUnit;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class DataProviderwithTestNG {

    
	@DataProvider(name = "Authentication")
	 
	  public static Object[][] credentials() {
	 
	        return new Object[][] { 
	        	{ Constant.Author,Constant.Country1 },
	        	{ Constant.Author,Constant.Country2 }
	        	};
	 	  }
	 
	  // Here we are calling the Data Provider object with its Name
	 
	  @Test(dataProvider = "Authentication")
	  
    public void testNoParameter(String author, String searchKey) throws InterruptedException{
      
        
        String exePath = "E:\\ChromeDriver\\chromedriver.exe";
	   	 System.setProperty("webdriver.chrome.driver", exePath);
	   	WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        ToolsQA.DemoMavenEclipseProject.Utility.Log.info("Opening Chrome Browser");
      
        driver.navigate().to("https://google.com");
        WebElement searchText = driver.findElement(By.name("q"));
        //Searching text in google text box
        searchText.sendKeys(searchKey);
        
        System.out.println("Welcome ->"+author+" Your search key is->"+searchKey);
                System.out.println("Thread will sleep now");
        
                ToolsQA.DemoMavenEclipseProject.Utility.Log.info("Web Applicaiton Launched");
        Thread.sleep(3000);
        System.out.println("Value in Google Search Box = "+searchText.getAttribute("value") +" ::: Value given by input = "+searchKey);
        //verifying the value in google search box
        
        AssertJUnit.assertTrue(searchText.getAttribute("value").equalsIgnoreCase(searchKey));
        driver.quit();
        ToolsQA.DemoMavenEclipseProject.Utility.Log.info("Browser closed");
}
}


