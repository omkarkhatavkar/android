
package ToolsQA.DemoMavenEclipseProject;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class MouseActions {
  @Test
  public void f() throws Exception {
	  String baseUrl = "http://www.facebook.com/"; 
	  String Path = "E:\\ChromeDriver\\chromedriver.exe";
	  System.setProperty("webdriver.chrome.driver", Path);
	  
	  WebDriver driver = new ChromeDriver();

	  driver.get(baseUrl);
	  WebElement txtUsername = driver.findElement(By.id("email"));
	  WebElement password = driver.findElement(By.id("pass"));

	  Actions builder = new Actions(driver);
	  Action seriesOfActions = builder.click(txtUsername)
			  .sendKeys("hello").
			  keyDown(txtUsername, Keys.CONTROL).
			  sendKeys("a").
			  release().
			  keyDown(Keys.CONTROL).
			  sendKeys("c").
			  release().
			  keyDown(password, Keys.CONTROL).
			  sendKeys("v").
			  keyUp(Keys.CONTROL).build();
		 
	 
	  seriesOfActions.perform() ;
	  
		
	   Thread.sleep(5000);

	  }
  }