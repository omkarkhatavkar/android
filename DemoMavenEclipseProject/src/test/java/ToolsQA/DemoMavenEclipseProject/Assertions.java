package ToolsQA.DemoMavenEclipseProject;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class Assertions {
  @Test
  public void f() {
	  String Path = "E:\\ChromeDriver\\chromedriver.exe";
	  System.setProperty("webdriver.chrome.driver", Path);
	  WebDriver driver = new ChromeDriver();
	  driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	  driver.manage().window().maximize();
	  driver.get("https://www.google.com");
	  String Title = driver.getTitle();
	 Assert.assertEquals(Title, "Google");
	
	
	SoftAssert soft = new SoftAssert();
	soft.assertEquals(Title, "Googl");
	soft.assertAll();
	System.out.println("Assertion passed");
	
	  driver.quit();
  }
}
