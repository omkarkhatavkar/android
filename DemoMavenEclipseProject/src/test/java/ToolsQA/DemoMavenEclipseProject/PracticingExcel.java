package ToolsQA.DemoMavenEclipseProject;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;

import org.apache.poi.xssf.usermodel.XSSFRow;

import org.apache.poi.xssf.usermodel.XSSFSheet;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.testng.annotations.Test;

public class PracticingExcel {
  @Test
  public void f() throws Exception {
	  String Path = "E:\\Selenium Tests\\MavenProject\\src\\main\\java\\ToolsQA\\DemoMavenEclipseProject\\Utility\\TestData.xlsx";
		 String SheetName = "Fruit";
		 
		 FileInputStream ExcelFile = new FileInputStream(Path);
		 XSSFWorkbook ExcelBook = new XSSFWorkbook(ExcelFile);
		 XSSFSheet ExcelSheet = ExcelBook.getSheet(SheetName);
		 
		 
		 int Rownum = ExcelSheet.getLastRowNum();
		 
		 for(int i=0;i<Rownum+1;i++){
			 
			int Columnnum = ExcelSheet.getRow(i).getLastCellNum();
			
			for(int j=0;j<Columnnum;j++) {
				System.out.print(ExcelSheet.getRow(i).getCell(j)+"\t");
			}
			System.out.println("\n");
		 }
		 
		 FileOutputStream ExcelFile2 = new  FileOutputStream(Path);
		 for(int k=1;k<Rownum+1;k++) {
			 XSSFRow row = ExcelSheet.getRow(k);
			 XSSFCell column = row.getCell(2);
			 column.setCellValue("Fail");
		 }
		 ExcelBook.write(ExcelFile2);
		 ExcelBook.close();
		 
  }
}
