package ToolsQA.DemoMavenEclipseProject;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class Alerting {
  @Test
  public void f() throws InterruptedException {
	  
	  String Path = "E:\\ChromeDriver\\chromedriver.exe";
	  System.setProperty("webdriver.chrome.driver", Path);
	  WebDriver driver = new ChromeDriver();	  
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  driver.manage().window().maximize();
	  driver.get("http://demo.guru99.com/test/delete_customer.php");
	   driver.findElement(By.name("cusid")).sendKeys("53920");
	   driver.findElement(By.name("submit")).click();
	   
	   Alert box = driver.switchTo().alert();
	   
	   String Notification = box.getText();
	   System.out.println(Notification);
	   Thread.sleep(5000);
	   box.accept();
	   Thread.sleep(5000);
	   box.accept();
	   driver.close();
	   
	   
  }
}
