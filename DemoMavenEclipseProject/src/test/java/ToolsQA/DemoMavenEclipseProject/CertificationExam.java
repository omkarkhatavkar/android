package ToolsQA.DemoMavenEclipseProject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.codehaus.plexus.util.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class CertificationExam {

	 String Path = "E:\\Selenium Tests\\MavenProject\\src\\main\\java\\ToolsQA\\DemoMavenEclipseProject\\Utility\\TestData.xlsx";
	 String SheetName = "Fruit";



  @Test 
  public void f() throws Exception   {
	  
	  FileInputStream fileread = new FileInputStream(Path);
	  XSSFWorkbook workbook = new XSSFWorkbook(fileread);
       XSSFSheet sheet = workbook.getSheet(SheetName);
       
       int lastrow = sheet.getLastRowNum();
       
       for(int i=0;i<lastrow+1;i++) {
    	   
    	   int lastcolumn = sheet.getRow(i).getLastCellNum();
    	   
    	   for(int j=0;j<lastcolumn;j++) {
    		   System.out.print(sheet.getRow(i).getCell(j).getStringCellValue() + "\t");
    	   }
    	   System.out.println();
       }
       
      
       
            
  }
       
}
