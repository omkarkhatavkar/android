package Mobile.appiumPractice;
import java.net.URL;
import java.util.List;
import java.net.MalformedURLException;
import org.testng.annotations.Test;
import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

public class BrowserStackAndroid {

  public static String userName = "omkarkhatavkar3";
  public static String accessKey = "N9enSTLPqm4C7yoFyKQb";

  @Test
  public void Test() throws MalformedURLException, InterruptedException {
    DesiredCapabilities caps = new DesiredCapabilities();
    
    caps.setCapability("browserName", "android");
   
    caps.setCapability("realMobile", "true");

    caps.setCapability("name", "omkarkhatavkar3's First Test");

    caps.setCapability("device", "Google Pixel 3");
    caps.setCapability("os_version", "9.0");
    caps.setCapability("project", "My  Project");
    caps.setCapability("build", "My First Build");
    caps.setCapability("name", "TestNG Android");

	
    AndroidDriver driver = new AndroidDriver(new URL("https://"+userName+":"+accessKey+"@hub-cloud.browserstack.com/wd/hub"), caps);
     driver.get("http://www.google.com");
    WebElement element = driver.findElement(By.name("q"));
    element.sendKeys("how to be happy");
    element.submit();
     	
    driver.quit();
  }
}
