package Mobile.appiumPractice;
//testing purpose ss

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class BaseClass {
	
	AppiumDriver<MobileElement> driver;
	
	@BeforeTest 
	public void setup() {
		
		try {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("deviceName","Redmi");
		capabilities.setCapability("platformName","Android");
		capabilities.setCapability("platformVersion","9 PKQ1.181203.001");
		capabilities.setCapability("udid","a13cc245");
		//capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT,"60");
		capabilities.setCapability("appPackage","com.miui.calculator");
		capabilities.setCapability("appActivity","com.miui.calculator.cal.CalculatorActivity");
		
		URL url = new URL("http://127.0.0.1:4723/wd/hub");
		driver = new AppiumDriver<MobileElement>(url, capabilities);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
		driver.findElement(By.id("android:id/button1")).click();
		driver.findElement(By.id("com.miui.calculator:id/btn_2_s")).click();
		driver.findElement(By.id("com.miui.calculator:id/btn_plus_s")).click();
		driver.findElement(By.id("com.miui.calculator:id/btn_3_s")).click();
		driver.findElement(By.id("com.miui.calculator:id/btn_equal_s")).click();
		String Output = driver.findElement(By.id("com.miui.calculator:id/result")).getText();
		
		System.out.println(Output);
		
		
		} catch (Exception e) {
			System.out.println("Cause is" + e.getCause());
			System.out.println("message is" + e.getMessage());
			e.printStackTrace();
		}

		
	}
	
  @Test
  public void f() {
	  
		
	  System.out.println("I am inside sample test");
  }
}
